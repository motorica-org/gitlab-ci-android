motorica-org/gitlab-ci-android
==============================
.. image:: https://gitlab.com/motorica-org/gitlab-ci-android/badges/master/build.svg
    :target: https://gitlab.com/motorica-org/gitlab-ci-android/commits/master
    :alt: Build status

Build Docker images for Android CI with GitLab CI. Built images are stored in GitLab Container Registry.

Example ``.gitlab-ci.yml``
--------------------------

.. code:: yaml

    image: registry.gitlab.com/motorica-org/gitlab-ci-android:master

    stages:
    - build

    before_script:
    - export GRADLE_USER_HOME=$(pwd)/.gradle
    - chmod +x ./gradlew

    cache:
      key: ${CI_PROJECT_ID}
      paths:
      - .gradle/

    build:
      stage: build
      script:
      - ./gradlew assembleDebug
      artifacts:
        paths:
        - app/build/outputs/apk/app-debug.apk

Can also be augmented with additional tools / frameworks / ... as demonstrated in `motorica-org/gitlab-ci-react-native-android <https://gitlab.com/motorica-org/gitlab-ci-react-native-android>`_ (a React Native Android CI image generator).

Originally based on `@jangrewe's work <https://github.com/jangrewe/gitlab-ci-android>`_.
