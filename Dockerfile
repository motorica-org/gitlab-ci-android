#
# GitLab CI: Android
#
# https://gitlab.com/motorica-org/gitlab-ci-android
#

FROM ubuntu:16.04
MAINTAINER Dmitriy Volkov <wldhx@wldhx.me>

ARG VERSION_SDK_TOOLS
ARG VERSION_BUILD_TOOLS
ARG VERSION_TARGET_SDK

ENV SDK_PACKAGES "build-tools-${VERSION_BUILD_TOOLS},android-${VERSION_TARGET_SDK},platform-tools,extra-android-m2repository,extra-android-support"

ENV ANDROID_HOME "/opt/android-sdk"
ENV PATH "$PATH:${ANDROID_HOME}/tools"
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -qq update && \
    apt-get install -qqy --no-install-recommends \
      curl \
      bsdtar \
      openjdk-8-jdk \
      libc6-i386 \
      lib32stdc++6 \
      lib32gcc1 \
      lib32ncurses5 \
      lib32z1 \
      openssh-client \
      git

RUN mkdir ${ANDROID_HOME} && \
    curl -s https://dl.google.com/android/repository/tools_r${VERSION_SDK_TOOLS}-linux.zip | bsdtar -xf - -C ${ANDROID_HOME} && \
    find ${ANDROID_HOME}/tools -type f -exec chmod +x '{}' \;

RUN echo y | ${ANDROID_HOME}/tools/android update sdk -u -a -t ${SDK_PACKAGES}

RUN mkdir -p $ANDROID_HOME/licenses/ && \
    echo "8933bad161af4178b1185d1a37fbf41ea5269c55" > $ANDROID_HOME/licenses/android-sdk-license && \
    echo "84831b9409646a918e30573bab4c9c91346d8abd" > $ANDROID_HOME/licenses/android-sdk-preview-license
